import java.util.List;
import java.util.Map;

public interface AnalyzingService {

	void analize(List<Player> opponents, Player player,
			Map<Integer, char[][]> archive, int gameRound);
}
