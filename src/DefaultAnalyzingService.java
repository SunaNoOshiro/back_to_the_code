import java.util.List;
import java.util.Map;

public class DefaultAnalyzingService implements AnalyzingService {

	private MoveService moveService = new DefaultMoveService();

	private Direction lastDirection;
	private Direction direction;

	private boolean back;
	
	@Override
	public void analize(List<Player> opponents, Player player,
			Map<Integer, char[][]> archive, int gameRound) {
		
		
		
		
		if (back && player.getBackInTimeLeft() > 0) {
			moveService.backRounds(25);
		} else {
			moveService.move(direction, player.getCurrentPosition());
		}
	}

}
