public class DefaultMoveService implements MoveService {
	@Override
	public void move(Direction direction, Point currentPosition) {
		if (direction.equals(Direction.LEFT)) {
			System.out.println((currentPosition.getX() - 1) + " "
					+ currentPosition.getY());
		}
		if (direction.equals(Direction.TOP)) {
			System.out.println(currentPosition.getX() + " "
					+ (currentPosition.getY() - 1));
		}
		if (direction.equals(Direction.RIGHT)) {
			System.out.println((currentPosition.getX() + 1) + " "
					+ currentPosition.getY());
		}
		if (direction.equals(Direction.BOTTOM)) {
			System.out.println(currentPosition.getX() + " "
					+ (currentPosition.getY() + 1));
		}
	}

	@Override
	public void move(Point point) {
		System.out.println(point.getX() + " " + point.getY());
	}

	@Override
	public void backRounds(int rounds) {
		 System.out.println("BACK " + rounds ); 
	}
}