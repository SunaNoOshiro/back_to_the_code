import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

class Game {
	
	private AnalyzingService analyzingService = new DefaultAnalyzingService();

	private List<Player> opponents;
	private Player me;	
	private Map<Integer,char[][]> archive = new HashMap<Integer, char[][]>();
	private Scanner in = new Scanner(System.in);

	private int gameRound;
	private int opponentCount;
	private char[][] map = new char[35][20];

	private void findOpponents() {
		opponents = new ArrayList<Player>();
		for (int i = 0; i < opponentCount; i++) {
			Player opp = new Player(new Point(in.nextInt(), in.nextInt()),
					in.nextInt());
			opponents.add(opp);
		}
	};

	private void findMe() {
		me = new Player(new Point(in.nextInt(), in.nextInt()), in.nextInt());
	};

	private void updateMap() {
		map = new char[35][20];
		for (int i = 0; i < 20; i++) {
			String line = in.next();
			map[i] = line.toCharArray();
			System.err.println(map[i]);
		}
		archive.put(gameRound, map);
	};

	public void startGame() {
		opponentCount = in.nextInt(); // Opponent count

		// game loop
		while (true) {
			gameRound = in.nextInt();
			System.err.println("gameRound:  " + gameRound);
			findMe();
			findOpponents();
			updateMap();
			
			analyzingService.analize(opponents, me, archive, gameRound);
			
		}
	}
}