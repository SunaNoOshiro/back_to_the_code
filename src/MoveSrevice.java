interface MoveService {

	void move(Direction direction, Point currentPosition);

	void move(Point point);

	void backRounds(int rounds);
}