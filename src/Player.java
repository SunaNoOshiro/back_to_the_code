class Player {

	private Point currentPosition;
	private int backInTimeLeft;

	public Player(Point currentPosition, int backInTimeLeft) {
		this.currentPosition = currentPosition;
		this.backInTimeLeft = backInTimeLeft;
	}

	public static void main(String args[]) {
		new Game().startGame();
	}

	public Point getCurrentPosition() {
		return currentPosition;
	}

	public int getBackInTimeLeft() {
		return backInTimeLeft;
	}
}